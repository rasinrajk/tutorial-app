let express = require('express');
let router = express.Router();
let auth = require('../middleware/middleware');
AuthController = require('../controllers/authentication');
const Usercontroller = require('../controllers/student');


//FOR COMMON POST
router.post('/login',AuthController.login);
router.post('/login-otp',AuthController.loginOtp);
router.post('/signup', AuthController.signup);
router.post('/logout', auth,AuthController.logout);
router.post('/forgot-password', AuthController.forgotPassword);
router.post('/reset/:token', AuthController.resetpassword);
router.post('/confirm-enable-2fa',auth,AuthController.confirmEnable2fa);

//FOR COMMON GET
router.get('/verify/:token',AuthController.mailverify);
router.get('/generate-qr',AuthController.generateqr);
router.get('/validate',AuthController.validate);
router.get('/enable-2fa',auth,AuthController.enable2fa);
router.post('/disable-2fa',AuthController.disable2fa);


// FOR STUDENT GET
router.get('/dash-board',auth,Usercontroller.dashBoard);


module.exports = router;

