let express = require('express');
let cors = require('cors');
let userRouter = require('./routers/studentApi');
let adminRouter = require('./routers/adminApi');
let port = process.env.PORT;

require('./db/db.js');
let app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/admin',adminRouter);
app.use('/',userRouter);

app.listen(port, function (){
    console.log(`app running on port ${port}`);
});
